lazy val root = (project in file("."))
  .settings(
    name := "DAFramework",
    scalaVersion := "2.11.12"
  )


  libraryDependencies ++= Seq(
      "org.apache.spark" %% "spark-core" % "2.3.2" ,
      "org.apache.spark" %% "spark-sql" % "2.3.2" ,
      "org.apache.spark" %% "spark-streaming-kafka-0-10" % "2.3.2",
      "org.apache.spark" %% "spark-streaming" % "2.3.2",
      "org.apache.phoenix" % "phoenix-spark" % "5.0.0-HBase-2.0",
      "org.apache.hadoop" % "hadoop-core" % "1.2.1",
      "org.apache.hbase" % "hbase" % "2.0.2" pomOnly(),
      "org.apache.hbase" % "hbase-client" % "2.0.2",
      "org.apache.hbase" % "hbase-common" % "2.0.2",
      "org.apache.hbase" % "hbase-server" % "2.0.2",
     
    )

    unmanagedJars in Compile += file("G:\\shc\\shc-core-1.1.1-2.1-s_2.11.jar")

   