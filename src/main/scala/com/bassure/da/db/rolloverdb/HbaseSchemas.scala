package com.bassure.da.db.rolloverdb

object HbaseSchemas {

  def rolloverActiveCatalogGetter(tableName:String) =
    s"""{
       |"table":{"namespace":"default", "name":"$tableName"},
       |"rowkey":"key",
       |"columns":{
       |"rowkey":{"cf":"rowkey", "col":"key", "type":"string"},
       |"tablename":{"cf":"atabledata", "col":"tablename", "type":"string"},
       |"starttime":{"cf":"atabledata", "col":"starttime", "type":"string"},
       |"endtime":{"cf":"atabledata", "col":"endtime", "type":"string"},
       |"prefix":{"cf":"atabledata", "col":"prefix", "type":"string"}
       |}
       |}""".stripMargin
  def rolloverInactiveCatalogGetter(tableName:String) =
    s"""{
       |"table":{"namespace":"default", "name":"$tableName"},
       |"rowkey":"key",
       |"columns":{
       |"rowkey":{"cf":"rowkey", "col":"key", "type":"string"},
       |"tablename":{"cf":"intabledata", "col":"tablename", "type":"string"}
       |}
       |}""".stripMargin


}
