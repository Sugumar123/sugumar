package com.bassure.da.db.rolloverdb


import java.io.IOException

import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.hadoop.hbase.TableName
import org.apache.hadoop.hbase.client.{ConnectionFactory, Delete, HTable, Put, Result, Row, Table}
import org.apache.spark.sql.functions.{col, udf}
import com.bassure.da.db._
import org.apache.spark.sql.{Dataset, Row, SparkSession}
import com.bassure.da.context.Context._
import java.text.SimpleDateFormat
import java.util.Date

import scala.tools.scalap.scalax.rules.StateRules

object RollOverDB {

  val ACTIVETABLE = "rollover_active2"
  val INACTIVETABLE = "rollover_inactive"
  var inactiveColumnFamilyName="intabledata"
  var activeColumnFamilyName="atabledata"

  var tableRecords: Dataset[org.apache.spark.sql.Row] = null

  var controllTableMap = scala.collection.mutable.Map[String, Map[String, String]]()

  var timeToLiveMap = scala.collection.mutable.Map[String, scala.collection.mutable.Map[String, String]]()


  var tableMap = scala.collection.mutable.Map[String, String]()

  val active_table = HbaseSchemas.rolloverActiveCatalogGetter(ACTIVETABLE)
  val inactive_table = HbaseSchemas.rolloverInactiveCatalogGetter(INACTIVETABLE)

  def initHbaseDF(tableName: String): Dataset[org.apache.spark.sql.Row] = {
    HBaseConnector.load(tableName)

  }
  def getSpark():SparkSession={
    com.bassure.da.context.Context.spark

  }

  val config = HBaseConfiguration.create()

  def rollHbase(tableName: String): Table = {
    val con = ConnectionFactory.createConnection(config)
    con.getTable(TableName.valueOf(tableName))

  }



  case class TableInfoRecord(rowkey: String, tablename: String, starttime: String, endtime: String, prefix: String,count:String,tempTime:String)

  def filterTables(current: String, old: String) = udf { (rowKey: String) => (rowKey.equals(current) || rowKey.equals(old)) }
  def filterStateCode(stateCode:String)=udf{(prefix:String)=>stateCode.equals(prefix)}

  def filterCurrentTable(current:String)=udf{(rowKey:String)=>rowKey.equals(current)}

  def getTable(tablePrefix: String): scala.collection.mutable.Map[String, Map[String, String]] = {
    tableRecords=initHbaseDF(active_table).filter(filterStateCode(tablePrefix)(col("prefix")))
    var current = tablePrefix + "_current"
    var old = tablePrefix + "_old"
    val tableData = tableRecords.filter(filterTables(current, old)(col("rowkey"))).select("tablename", "starttime", "endtime").collect
    tableData.map(row => makeMap(row.getString(0), row.getString(1), row.getString(2)))
    println("Map "+controllTableMap)
    return controllTableMap
  }

  def getCurentTableInfo(stateCode:String):Array[org.apache.spark.sql.Row]={
    tableRecords.filter(filterCurrentTable(stateCode+"current")(col("rowkey"))).select("count").collect()
  }
  def insertCountAndEndtime(stateCode:String,count:String,endTime:String):Unit={
    var currentTable=getCurentTableInfo(stateCode)
    var record:TableInfoRecord=null
    if(currentTable(0).getString(6).toLong > endTime.toLong) {
      record = TableInfoRecord(currentTable(0).getString(0), currentTable(0).getString(1), currentTable(0).getString(2), currentTable(0).getString(3), currentTable(0).getString(4),count,endTime)
    }else {
      record = TableInfoRecord(currentTable(0).getString(0), currentTable(0).getString(1), currentTable(0).getString(2), currentTable(0).getString(3), currentTable(0).getString(4), count, currentTable(0).getString(6))
    }
    var recordArray = new Array[TableInfoRecord](3)
    recordArray(0) = record
    var records = recordArray.toSeq
    val spark =getSpark()
    import spark.implicits._
    val data = spark.sparkContext.parallelize(records).toDF()


    HBaseConnector.store(active_table, data)


  }

def getCountofTable(stateCode:String):String={
  var count=getCurentTableInfo(stateCode)
  count(0).getString(0)


}




  def makeMap(tableName: String, startTime: String, endTime: String): Unit = {
    if (endTime == " ") {
      var currentTablesMap = Map[String, String]()
      currentTablesMap += tableName -> startTime
      controllTableMap += "current" -> currentTablesMap

    }
    else {
      var oldTablesMap = Map[String, String]()
      oldTablesMap += tableName -> endTime
      controllTableMap += "old" -> oldTablesMap

    }
  }
  def updateRolloverTables(stateCode: String, emptyTables: Array[String]): Unit = {
    tableFromInactiveToActive(emptyTables, stateCode)
    updateTable(stateCode)


  }

  //Need to change the generic name
  def tableFromInactiveToActive(emptyTables: Array[String], stateCode: String): Unit = {
    if (emptyTables.length != 0) {
      if (activeEmptyTableRemover(emptyTables, ACTIVETABLE)) {
        inactiveTableAdder(emptyTables, INACTIVETABLE, stateCode)

      }
    }

  }
  def inactiveTableAdder(emptyTables: Array[String], controllTable: String, stateCode: String) = {
    var rolloverInactive = rollHbase(controllTable)
    try{
      for (l <- 0 to emptyTables.length - 1) {
        if (!(getTable(stateCode)("old").keys.head.equals(emptyTables(l)))) {
        val putter = new Put(emptyTables(l).getBytes())
        putter.addColumn(inactiveColumnFamilyName.getBytes(), "tablename".getBytes(), emptyTables(l).getBytes())
        rolloverInactive.put(putter)
        if (!(getTable(stateCode)("old").keys.head.equals(emptyTables(l)))) {
          tableMap.remove(emptyTables(l))
        }
        }
      }
      true
    }catch{
      case e: IOException =>{
        false
      }
    }
  }
  def activeEmptyTableRemover(emptyTables: Array[String], controllTable: String) = {
    var rolloverActive = rollHbase(controllTable)
    try{
      for (e <- 0 to emptyTables.length - 1) {
        val del = new Delete(emptyTables(e).getBytes())
        del.addFamily(activeColumnFamilyName.getBytes())
        rolloverActive.delete(del)
      }
      true
    }catch{
      case e: IOException =>{
        false
      }
    }
  }


  def getStartTime(endTime: String): String = {
    var startTime: Long = endTime.toLong
    startTime += 1
    return startTime.toString

  }

  def getCurrentTime(): String = {
    val formatter = new SimpleDateFormat("yyMMddHHmmss")
    val date = new Date()
    formatter.format(date)
  }


  def filterTable(currentTable: String) = udf { tableName: String => tableName.equals(currentTable) }

  def updateTable(tablePrfix: String): Unit = {


    //Multiple times, hitting database
    val tData: scala.collection.mutable.Map[String, Map[String, String]] = getTable(tablePrfix)
    val currentTableName = tData("current").keys.head
    val oldTableName = tData("old").keys.head

    val tableDataCurrent = tableRecords.filter(filterTable(currentTableName)(col("tablename"))).collect
    val tableDataOld1 = tableRecords.filter(filterTable(oldTableName)(col("tablename"))).collect
    val tableDataNew = tableDataCurrent(0)
    val tableDataOld = tableDataOld1(0)


    val record1 = TableInfoRecord(tableDataOld.getString(1), tableDataOld.getString(1), tableDataOld.getString(2), tableDataOld.getString(3), tableDataOld.getString(4),tableDataOld.getString(5),tableDataOld.getString(6))
    val record2 = TableInfoRecord(tableDataOld.getString(0), tableDataNew.getString(1), tableDataNew.getString(2), tableDataNew.getString(6), tableDataNew.getString(4),tableDataNew.getString(5),tableDataNew.getString(6))
    val record3 = TableInfoRecord(tableDataNew.getString(0), getNewTable(tablePrfix), getStartTime(tableDataNew.getString(6)), " ", tableDataNew.getString(4),"0","0")



    //record1=TableInfoRecord(tn_01,tn_01,starttime,endtime,tn)
    //record2=Ta(tn_old,tn_02,starttime,20000000,tn)
    //Map(tn->Map(tn_02->200529101341,tn_03->200529101341))          currentServerTime= 200612101341-14days>=200529101341


    var recordArray = new Array[TableInfoRecord](3)
    recordArray(0) = record1
    recordArray(1) = record2
    recordArray(2) = record3

    //    var recordSeq=Seq()
    //    recordSeq=recordSeq

    var records = recordArray.toSeq
    val spark = getSpark()
    import spark.implicits._
    val data = spark.sparkContext.parallelize(records).toDF()


    HBaseConnector.store(active_table, data)


    tableMap += tableDataNew.getString(1) -> getCurrentTime()
    timeToLiveMap += tablePrfix -> tableMap



  }

  def filterNewTable(currentTable: String) = udf { tableName: String => tableName.equals(currentTable) }

  def getNewTable(tablePrefix: String): String = {

    val noOfTables = 10
    //We already have map to find current table Name
    val tableName = getTable(tablePrefix)("current").keys.head

    var temp: Array[String] = tableName.split("_")

    var tableNo = (temp(1).toInt % noOfTables) + 1

    var tName = ""

    if (tableNo == 10) {
      tName = tablePrefix + "_" + tableNo.toString
    }
    else {
      tName = tablePrefix + "_0" + tableNo.toString
    }


    println(tName)

    if (RemoveTableFromInactive(tName, INACTIVETABLE)) tName else "No table is there"

  }

  def RemoveTableFromInactive(tableName: String, controllTable: String): Boolean = {
    var rolloverInactive = rollHbase(controllTable)
    val del = new Delete(tableName.getBytes())
    del.addFamily("intabledata".getBytes())
    rolloverInactive.delete(del)
    return true
  }

  /** this method will add the table info into the inactive table from Seq which is passed as an argument */


  //  def filterStateTables(stateCode: String) = udf { prefix: String => prefix.equals(stateCode) }
  //
  //  def removeCurrentAndOld(current: String, old: String) = udf { rowkey: String => !(rowkey.equals(current) || rowkey.equals(old)) }

  def getListOfTables(stateCode: String) = {

    if ((timeToLiveMap.count(z => true)) == 0) {

      0
    }
    else {
      timeToLiveMap(stateCode)
    }


  }
  //  def filterSearchTables(startTime:Long,endTime:Long)=udf {(tableStarts:String,tableEnds:String)=>(endTime>=tableStarts.toLong)||(startTime<=tableEnds.toLong)||((startTime>=tableStarts.toLong)&&(endTime<=tableEnds.toLong))}
  //  def filterStateCode(stateCode:String)=udf{(prefix:String)=>stateCode.equals(prefix)}
  //  def getSearchTables(stateCode:String,startTime:String,endTime:String):Array[org.apache.spark.sql.Row]={
  //    tableRecords.filter(filterStateCode(stateCode)(col("prefix"))).filter(filterSearchTables(startTime.toLong,endTime.toLong)(col("startTime"),col("endTime"))).collect()
  //  }

}