package com.bassure.da.db


import org.apache.spark.sql.execution.datasources.hbase.HBaseTableCatalog
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import com.bassure.da.context.Context._

object HBaseConnector {

    def store(schema:String,data:Dataset[Row]):Unit = {
        data.write.options(Map(HBaseTableCatalog.tableCatalog -> schema,HBaseTableCatalog.newTable -> "8")).format(hbaseFormat).save()
    }
    def load(schema:String):Dataset[Row]={
        spark.read.options(Map(HBaseTableCatalog.tableCatalog->schema)).format(hbaseFormat).load()

    }

}