package com.bassure.da.db

import com.bassure.da.context.Context._
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.phoenix.spark._

object PhoenixConnector {

    def store(table:String,data:Dataset[Row]):Unit = {
        data.write.options(Map("table"->table,"zkUrl" -> phoenixConfig("phoenix.zookeeperurl"))).format(phoenixConfig("phoenix.driver")).mode("overwrite").save()
    }
    def load(table:String):Dataset[Row]={
        spark.read.options(Map("table"->table,"zkUrl" -> phoenixConfig("phoenix.zookeeperurl"))).format(phoenixConfig("phoenix.driver")).load()

    }


}