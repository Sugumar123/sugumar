package com.bassure.da.util

import scala.io.Source
import java.io.File

/** This scala object process the properties file and make the file data into Immutable Map  */

object ConfigLoader {
  var maps = collection.immutable.Map[String, String]();

  /** this method would be invoked by fileMapper method which is present in same class */
  /** This method takes string as parameter(Single parameter) and make the string into key value pair
   * as well as add that key value data to same Immutable Map object */


  def splitting(data: String): Unit = {
    var temp: Array[String] = data.split("=");

    maps += temp(0) -> temp(1)
  }

  /** This method takes single parameter as string, which is nothing but Properties file name that client want's to process  
   This method takes each line in the properties file as string
    and send it to splitting method as a parameter which is present in same class 
  At the end, this method returns the  Immutable Map object  */

  def load(fileName: String): Map[String, String] = {
   // Source.fromInputStream(getClass.getClassLoader.getResourceAsStream(fileName))
   Source.fromFile(new File(fileName)).getLines().toList.map(splitting(_));
    return maps
  }


}
