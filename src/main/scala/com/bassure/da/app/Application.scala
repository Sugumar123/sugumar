package com.bassure.da.app

trait Application {
    def initialize(cmd:Array[String]):Unit
    def execute():Unit
}