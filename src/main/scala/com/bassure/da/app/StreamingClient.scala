package com.bassure.da.app

import com.bassure.da.context.Context._
import com.bassure.da.util.ConfigLoader
import com.bassure.da.messaging.KafkaConnector
import org.apache.spark.sql.SparkSession
import org.apache.spark.streaming.{Minutes, StreamingContext}
import scala.reflect.runtime.universe


object StreamingClient extends Application{

     def initialize(args:Array[String])= {
        spark = SparkSession.builder().getOrCreate()
        val config = ConfigLoader.load(args(1))
        init(config)
        

    }

     def execute():Unit = {
        val ssc = new StreamingContext(spark.sparkContext,Minutes(messageConfig("message.time").toLong))
        ssc.checkpoint(messageConfig("message.checkpoint"))
        val data = KafkaConnector.readStream(ssc).map(row=>row.value())
     /*   var tempData:Any=data
        flow.foreach(op=>
              
            if ( businessImpl contains op){
                tempData = businessImpl(op).asInstanceOf[BusinessTrait].execute(tempData)
            }else {
               // val impl = Class.forName(businessOp(op)).newInstance().asInstanceOf[BusinessTrait]
                val runtimeMirror = universe.runtimeMirror(getClass.getClassLoader)
                val module = runtimeMirror.staticModule(businessOp(op))
                val obj = runtimeMirror.reflectModule(module)
                val app:BusinessTrait = obj.instance.asInstanceOf[BusinessTrait]
                businessImpl +=  op->app
                tempData=app.execute(tempData)
            }
        )
       */ 

       data.foreachRDD{(rdd)=>
            if(!rdd.isEmpty){
                var tempData:Any=rdd.repartition(partitions)
                flow.foreach(op=>
              
                if ( businessImpl contains op){
                    tempData = businessImpl(op).asInstanceOf[BusinessTrait].execute(tempData)
                }else {
                 // val impl = Class.forName(businessOp(op)).newInstance().asInstanceOf[BusinessTrait]
                    val runtimeMirror = universe.runtimeMirror(getClass.getClassLoader)
                    val module = runtimeMirror.staticModule(businessOp(op))
                    val obj = runtimeMirror.reflectModule(module)
                    val app:BusinessTrait = obj.instance.asInstanceOf[BusinessTrait]
                    businessImpl +=  op->app
                    tempData=app.execute(tempData)
                }
            
            )
            }
        } 
        


        ssc.start()
        ssc.awaitTermination()


    }
    

}