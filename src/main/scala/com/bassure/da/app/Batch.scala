package com.bassure.da.app

import com.bassure.da.context.Context._
//import com.bassure.da.app.Application
import org.apache.spark.sql.SparkSession
import com.bassure.da.util.ConfigLoader
import scala.reflect.runtime.universe

object Batch extends Application {

    def initialize(args:Array[String]):Unit= {
        spark = SparkSession.builder().getOrCreate()
        val config = ConfigLoader.load(args(1))
        init(config)
        
    }

    def execute():Unit= {
        var tempData :Any= null
        flow.foreach(op=>
            if ( businessImpl contains op){
                    tempData = businessImpl(op).asInstanceOf[BusinessTrait].execute(tempData)
                }else {
                 // val impl = Class.forName(businessOp(op)).newInstance().asInstanceOf[BusinessTrait]
                    val runtimeMirror = universe.runtimeMirror(getClass.getClassLoader)
                    val module = runtimeMirror.staticModule(businessOp(op))
                    val obj = runtimeMirror.reflectModule(module)
                    val app:BusinessTrait = obj.instance.asInstanceOf[BusinessTrait]
                    businessImpl +=  op->app
                    tempData=app.execute(tempData)
                }

        )
        
        
    }
    
}