package com.bassure.da

import com.bassure.da.app.Application
import scala.reflect.runtime.universe

object Main {
    def main(args:Array[String]) {

        val implClass = args(0)
        try {
            val runtimeMirror = universe.runtimeMirror(getClass.getClassLoader)
            val module = runtimeMirror.staticModule(implClass)
            val obj = runtimeMirror.reflectModule(module)
            val app:Application = obj.instance.asInstanceOf[Application]  
            //val app = Class.forName(implClass+"$").asInstanceOf[Application]
            app.initialize(args)
            app.execute()
        }catch {
            case e: ClassNotFoundException => print("Could not load the class:"+implClass)
        }
    }
}
