package com.bassure.da.messaging

import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.streaming.kafka010._
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.dstream.DStream


import com.bassure.da.context.Context._

object KafkaConnector {

     def kafaparams() = {
        Map(
            "bootstrap.servers" -> messageConfig("message.bootstrapservers"),
            "key.deserializer" -> classOf[StringDeserializer],
            "value.deserializer" -> classOf[StringDeserializer],
            "group.id" -> messageConfig("message.groupid")
        
        )
     }

        def readStream(ssc:StreamingContext)={
            var topics:List[String]=null
            if(messageConfig("message.topics").contains(",")){
                topics= messageConfig("message.topics").split(",").toList
            }else {
                topics = List(messageConfig("message.topics"))
            }
            
            KafkaUtils.createDirectStream[String,String](ssc,PreferConsistent,Subscribe[String,String](topics,kafaparams()))
        }

}