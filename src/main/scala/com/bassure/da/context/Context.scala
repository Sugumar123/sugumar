package com.bassure.da.context

import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

object Context {
    var spark:SparkSession = null
    var flow:List[String] = null
    var businessOp:scala.collection.mutable.Map[String,String] = scala.collection.mutable.Map[String,String]()
    var businessImpl:scala.collection.mutable.Map[String,Any]=scala.collection.mutable.Map[String,Any]()
    var hbaseFormat:String = null
    var partitions:Int=2
    var messageConfig:scala.collection.mutable.Map[String,String]=scala.collection.mutable.Map[String,String]()
    var phoenixConfig:scala.collection.mutable.Map[String,String]=scala.collection.mutable.Map[String,String]()
    var businessData:scala.collection.mutable.Map[String,String]=scala.collection.mutable.Map[String,String]()


    
    def init(config:Map[String,String]):Unit = {

        if (config("flow").contains(",")) {
            flow = config("flow").split(",").toList
        }else {
            flow = List(config("flow"))
        }
        println(flow)
        flow.foreach(op=>
            businessOp +=  op->config(op)
        ) 

        setConfig("business",config,businessData)
        setConfig("message",config,messageConfig)
        setConfig("phoenix",config,phoenixConfig)
        hbaseFormat = config("hbaseformat")
        partitions = config("partitions").toInt

    }

    def setConfig(typeName:String,configMap:Map[String,String],targetConfig:scala.collection.mutable.Map[String,String]):Unit = {
        configMap.foreach((row)=>{
            if(row._1.contains(typeName)){
                targetConfig += row._1->row._2
            }
        }
        )

    }

}